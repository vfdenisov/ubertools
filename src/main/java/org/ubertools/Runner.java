package org.ubertools;

import io.vertx.core.Launcher;

/**
 * @author Viktor
 * @since 2017-02-01
 */
public class Runner extends Launcher {
    public static void main(final String[] args) {
        if (System.getProperty("vertx.logger-delegate-factory-class-name") == null) {
            System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
        }
        final Runner runner = new Runner();
        runner.dispatch(runner, args);
    }
}
