package org.ubertools.deployments;

import io.vertx.core.*;
import lombok.extern.slf4j.Slf4j;
import org.ubertools.annotations.DeploySequentially;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author Viktor
 * @since 2017-02-01
 */
@Slf4j
public class ParallelDeployer extends AbstractDeployer {
    public ParallelDeployer(final List<Class<? extends AbstractVerticle>> verticleClasses, final DeploymentOptions deploymentOptions, final Vertx vertx) {
        super(verticleClasses, deploymentOptions, vertx);
    }

    public void handleDeployments(final Future<Void> startFuture) {
        final List<Future> parallelDeployments = doParallelDeployments();
        checkForSuccessParDeploymentCompletion(startFuture, parallelDeployments);

    }

    private List<Future> doParallelDeployments() {
        final List<Class<? extends AbstractVerticle>> parallelDeployments = verticleClasses.stream()
            .filter(cls -> !cls.isAnnotationPresent(DeploySequentially.class))
            .collect(toList());
        log.info("Parallel to deploy: {}", parallelDeployments.size());

        return deployParallel(parallelDeployments, deploymentOptions);
    }

    private List<Future> deployParallel(
        final List<Class<? extends AbstractVerticle>> parallelDeployments, final DeploymentOptions deploymentOptions
    ) {
        return parallelDeployments.stream().map(cls -> {
            final Future<Void> f = Future.future();
            log.info("Deploying: {}", cls.getSimpleName());
            doDeployVerticle(cls, deploymentOptions, f);
            return f;
        }).collect(toList());
    }

    private void checkForSuccessParDeploymentCompletion(
        final Future<Void> startFuture, final List<Future> deploymentCompletions
    ) {
        CompositeFuture.all(deploymentCompletions).setHandler(h -> {
            if (h.succeeded()) {
                log.info("All parallel deployments completed. Total async deployment count: {}", deploymentCompletions.size());
                startFuture.complete();
            } else {
                log.warn("Some parallel deployments ended with errors.", h.cause());
                startFuture.fail(h.cause());
            }
        });
    }
}
