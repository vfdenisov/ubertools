package org.ubertools.verticles.abstracts;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * @author Viktor
 * @since 2017-02-01
 */
public abstract class BaseVerticle extends AbstractVerticle {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
}
