package org.ubertools.annotations;

import java.lang.annotation.*;

/**
 * @author Viktor
 * @since 2017-02-01
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface Exclude {
}
