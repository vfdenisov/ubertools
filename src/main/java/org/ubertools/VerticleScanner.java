package org.ubertools;

import io.vertx.core.AbstractVerticle;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.ubertools.annotations.Exclude;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.nio.file.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Viktor
 * @since 2017-02-01
 */
@Slf4j
@AllArgsConstructor
public class VerticleScanner {

    private final String packageToScan;
    private final String scanningRoot;

    public VerticleScanner(final String packageToScan) {
        this.packageToScan = packageToScan;
        this.scanningRoot = packageToScan.split("\\.")[0];

    }

    public List<Class<? extends AbstractVerticle>> findVerticleClasses() {
        try {
            final URI uri = this.getClass().getResource("/" + this.getClass().getCanonicalName().replaceAll("\\.", "/") + ".class").toURI();
            final String[] splitPath = uri.toString().split("!");
            return processPath(uri, splitPath);
        } catch (final Exception ex) {
            log.error("Error when scanning for verticles", ex);
            return Collections.emptyList();
        }
    }

    private List<Class<? extends AbstractVerticle>> processPath(final URI uri, final String[] splitPath) throws IOException {
        final Map<String, String> env = new HashMap<>();
        if (isWithinJar(splitPath)) {
            return processJarFile(env, splitPath);
        } else {
            return processFilesystemPath(uri);
        }
    }

    private boolean isWithinJar(final String[] splitPath) {
        return splitPath.length == 2; //splitting path by '!' yields two items if path targets something withing jar file.
    }

    private List<Class<? extends AbstractVerticle>> processJarFile(final Map<String, String> env, final String[] splitPath) throws IOException {
        final String parentPath = splitPath[0];
        log.debug("Jar file path: {}", parentPath);
        try (final FileSystem fs = FileSystems.newFileSystem(URI.create(parentPath), env)) {
            return handleFiles(fs.getPath(splitPath[1]).getRoot());
        }
    }

    private List<Class<? extends AbstractVerticle>> processFilesystemPath(final URI uri) throws IOException {
        Path result = Paths.get(uri);
        while (!result.toString().endsWith(scanningRoot)) {
            result = result.getParent();
        }

        final Path parent = result.getParent();// one more step to include 'com' package in scan
        log.debug("Filesystem file path: {}", parent);
        return handleFiles(parent);
    }

    private List<Class<? extends AbstractVerticle>> handleFiles(final Path resourcePath) throws IOException {
        log.info("Running scan from: {}", resourcePath);
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final Pattern regex = Pattern.compile("[\\" + File.separator + "]|[/]");
        final boolean parentIsSeparator = resourcePath.toString().equals("/") || resourcePath.toString().equals(File.separator);

        final List<Class<?>> verticleClasses = StreamEx.of(Files.walk(resourcePath))
            .parallel()
            .map(Path::toString)
            .map(path -> parentIsSeparator ? path : path.replace(resourcePath.toString(), "")) //strip leading parent path is it is not "\" or "/"
            .map(path -> regex.matcher(path).replaceAll(".")) //replace separating symbols with .
            .filter(path -> path.endsWith("class") && path.contains(packageToScan)) //scan only for compiled classes
            .map(path -> path.substring(0, path.lastIndexOf("."))) //strip '.class'
            .map(classPath -> loadClass(classLoader, classPath))
            .filter(AbstractVerticle.class::isAssignableFrom) //leave only our verticles
            .remove(cls -> Modifier.isAbstract(cls.getModifiers()) || cls.isAnnotationPresent(Exclude.class)) //exclude abstract classes and classes designated as excluded from the list
            .collect(Collectors.toList());

        return resolveTypeCasts(verticleClasses);
    }

    @SuppressWarnings("unchecked")
    private List<Class<? extends AbstractVerticle>> resolveTypeCasts(final List<Class<?>> verticleClasses) {
        final List<Class<? extends AbstractVerticle>> result = new ArrayList<>();
        for (final Class cls : verticleClasses) result.add(cls);
        return result;
    }

    private Class<?> loadClass(final ClassLoader cl, final String classPath) {
        try {
            return Class.forName(classPath.substring(1), false, cl);
        } catch (final ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unused")
    private void tstScan(final Path resourcePath, final Pattern regex, final boolean parentIsSeparator) throws IOException {
        log.info("Sep: {}, empty: {}", File.separator, File.separator.trim().isEmpty());
        log.info("Res: {} ", StreamEx.of(Files.walk(resourcePath))
            .parallel()
            .map(Path::toString)
            .map(path -> parentIsSeparator ? path : path.replace(resourcePath.toString(), "")) //strip leading parent path is it is not "\" or "/"
            .map(path -> File.separator.trim().isEmpty() ? path : regex.matcher(path).replaceAll(".")) //replace separating symbols with
            .filter(path -> path.endsWith("class") && path.contains(packageToScan)) //scan only
//            .map(path -> path.substring(0, path.lastIndexOf("."))) //strip '.class'
            .collect(Collectors.toList()));
    }
}

