package org.ubertools.verticles;

import io.vertx.core.Context;
import io.vertx.core.Vertx;
import org.ubertools.annotations.DeploySequentially;
import org.ubertools.verticles.abstracts.BaseVerticle;

/**
 * @author Viktor
 * @since 2017-02-01
 */
@DeploySequentially
public class BasicVerticle extends BaseVerticle {
    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        log.info("Deployed: {}", this.deploymentID());
    }
}
