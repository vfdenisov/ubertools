package org.ubertools.deployments;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import lombok.extern.slf4j.Slf4j;
import org.ubertools.annotations.DeploySequentially;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * @author Viktor
 * @since 2017-02-01
 */
@Slf4j
public class SequentialDeployer extends AbstractDeployer {

    public SequentialDeployer(final List<Class<? extends AbstractVerticle>> verticleClasses, final DeploymentOptions deploymentOptions, final Vertx vertx) {
        super(verticleClasses, deploymentOptions, vertx);
    }

    public Future<Void> handleDeployments() {
        final List<Class<? extends AbstractVerticle>> sequentialDeployments = verticleClasses.stream()
            .filter(cls -> cls.isAnnotationPresent(DeploySequentially.class))
            .collect(toList());
        log.info("Sequential to deploy: {}", sequentialDeployments.size());
        final List<Future<Void>> sequential = deploySequentially(sequentialDeployments, deploymentOptions);
        final Future<Void> f = Future.future();
        checkForSuccessSeqDeploymentCompletion(f, sequential);
        return f;
    }

    private List<Future<Void>> deploySequentially(
        final List<Class<? extends AbstractVerticle>> verticleClasses, final DeploymentOptions deploymentOptions
    ) {
        final List<Future<Void>> f =
            IntStream.rangeClosed(0, verticleClasses.size()).mapToObj(cls -> Future.<Void>future()).collect(toList());

        IntStream.range(0, f.size() - 1).forEachOrdered(i -> {
            final Future<Void> voidFuture = f.get(i);
            voidFuture.compose(
                onComplete -> doDeployVerticle(verticleClasses.get(i), deploymentOptions, f.get(i + 1)), f.get(i + 1)
            );
        });
        f.get(0).complete(); //start sequential deployment
        return f;
    }

    private void checkForSuccessSeqDeploymentCompletion(
        final Future<Void> startFuture, final List<Future<Void>> deploymentCompletions
    ) {
        final Future<Void> voidFuture = deploymentCompletions.get(deploymentCompletions.size() - 1);

        voidFuture.setHandler(event -> {
            if (event.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(event.cause());
            }
        });
    }
}
