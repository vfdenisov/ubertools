package org.ubertools.deployments;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author Viktor
 * @since 2017-02-01
 */
@Slf4j
@AllArgsConstructor
abstract class AbstractDeployer {
    final List<Class<? extends AbstractVerticle>> verticleClasses;
    final DeploymentOptions deploymentOptions;
    private final Vertx vertx;

    void doDeployVerticle(
        final Class<? extends AbstractVerticle> cls, final DeploymentOptions deploymentOptions, final Future<Void> f
    ) {
        log.info("Deploying: {}", cls.getSimpleName());
        vertx.deployVerticle(cls.getName(), deploymentOptions, h -> {
            log.debug("Deployment result: {} for {}", h.result(), cls.getSimpleName());
            if (h.succeeded()) {
                f.complete();
            } else {
                f.fail(h.cause());
            }
        });
    }
}
