package org.ubertools.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.ubertools.VerticleScanner;
import org.ubertools.annotations.Exclude;
import org.ubertools.deployments.ParallelDeployer;
import org.ubertools.deployments.SequentialDeployer;
import org.ubertools.verticles.abstracts.BaseVerticle;

import java.util.List;

/**
 * @author Viktor
 * @since 2017-02-01
 */
@Exclude
public class MainVerticle extends BaseVerticle {
    private final static Logger logger = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(final Future<Void> startFuture) throws Exception {
        final List<Class<? extends AbstractVerticle>> verticleClasses
            = new VerticleScanner(config().getString("packages.to.scan", "com.luxoft")).findVerticleClasses();
        deployFoundVerticles(verticleClasses, startFuture);
    }

    private void deployFoundVerticles(final List<Class<? extends AbstractVerticle>> verticleClasses, final Future<Void> startFuture) {
        final DeploymentOptions deploymentOptions = new DeploymentOptions().setConfig(config());

        final Future<Void> seqCompletionFuture = new SequentialDeployer(verticleClasses, deploymentOptions, vertx).handleDeployments();
        seqCompletionFuture.setHandler(h -> {
            if (h.succeeded()) {
                logger.info("All sequential deployments completed. Starting with parallel deployments.");
                new ParallelDeployer(verticleClasses, deploymentOptions, vertx).handleDeployments(startFuture);
            } else {
                logger.warn("Some parallel deployments ended with errors.", h.cause());
                startFuture.fail(h.cause());
            }

        });
    }
}

